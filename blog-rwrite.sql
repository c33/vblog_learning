
DROP database IF EXISTS `blog`;
CREATE DATABASE `blog` DEFAULT CHARACTER SET utf8;

USE `blog`;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT comment '文章ID',
  `title` varchar(255) not null DEFAULT '' comment '标题',
  `md_content` text COMMENT 'md文件源码',
  `html_content` text COMMENT 'html源码',
  `summary` text comment '概要',
  `cid` int(11) DEFAULT NULL comment '类别ID',
  `uid` int(11) DEFAULT NULL comment '作者ID',
  `create_time` datetime not null default CURRENT_TIMESTAMP comment '创建时间',
  `update_time` datetime not null DEFAULT CURRENT_TIMESTAMP on update current_timestamp comment '更新时间',
  `status` int(11) DEFAULT NULL COMMENT '0表示草稿箱，1表示已发表，2表示已删除',
  `page_view` int(11) DEFAULT '0' comment '浏览量',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`) using btree,
  Key `cid`(`cid`) using btree,
  KEY `title`(`title`) using btree,
  Key `status`(`status`) using btree
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for article_tags
-- ----------------------------
DROP TABLE IF EXISTS `article_tags`;
CREATE TABLE `article_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) DEFAULT NULL comment '文章ID',
  `tid` int(11) DEFAULT NULL comment '标签ID',
  PRIMARY KEY (`id`),
  KEY `tid` (`tid`) using btree,
  KEY `aid` (`aid`) using btree
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) not null DEFAULT '' comment '类别名称',
  `create_time` datetime DEFAULT current_timestamp comment '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) DEFAULT NULL comment '评论的文章id',
  `content` text comment '评论内容',
  `create_time` datetime not null DEFAULT current_timestamp comment '评论时间',
  `parent_id` int(11) DEFAULT NULL COMMENT '-1表示正常回复，其他值表示是评论的回复',
  `uid` int(11) DEFAULT NULL comment '评论用户的id',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`) using btree,
  KEY `uid` (`uid`) using btree,
  KEY `parent_id` (`parent_id`) using btree
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL comment '角色英文名称',
  `zh_name` varchar(32) DEFAULT NULL comment '角色中文名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1','admin', '超级管理员');
INSERT INTO `roles` VALUES ('2', 'user','普通用户');


-- ----------------------------
-- Table structure for roles_user
-- ----------------------------
DROP TABLE IF EXISTS `roles_user`;
CREATE TABLE `roles_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(11) DEFAULT '2'comment '角色id',
  `uid` int(11) DEFAULT NULL comment '用户ID',
  PRIMARY KEY (`id`),
  KEY `rid` (`rid`) using btree,
  KEY `roles_user_ibfk_2` (`uid`) using btree
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roles_user
-- ----------------------------
INSERT INTO `roles_user` VALUES ('1', '1', '2');
INSERT INTO `roles_user` VALUES ('2', '2', '1');
INSERT INTO `roles_user` VALUES ('3', '2', '2');
INSERT INTO `roles_user` VALUES ('4', '2', '3');
INSERT INTO `roles_user` VALUES ('5', '2', '4');
INSERT INTO `roles_user` VALUES ('6', '2', '5');
INSERT INTO `roles_user` VALUES ('7', '2', '6');
INSERT INTO `roles_user` VALUES ('8', '2', '7');
INSERT INTO `roles_user` VALUES ('9', '2', '8');
INSERT INTO `roles_user` VALUES ('10', '2', '9');
INSERT INTO `roles_user` VALUES ('11', '2', '10');
INSERT INTO `roles_user` VALUES ('12', '2', '11');
-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL comment '用户ID',
  `name` varchar(32) DEFAULT NULL comment '标签名称',
  PRIMARY KEY (`id`),
  Key `uid`(`uid`) using btree,
  KEY `name` (`name`) using btree
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


drop table if exists `article_tags`;
create table `article_tags`(
`id` int(11) not null auto_increment,
`aid` int(11) default null comment '文章ID',
`tid` int(11) default null comment '标签ID',
primary key(`id`),
key `aid`(`aid`) using btree
)engine=InnoDB default charset=utf8;
-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) DEFAULT NULL,
  `nickname` varchar(64) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `mobile` char(11) default '',
  `enabled` tinyint(1) DEFAULT '1',
  `email` varchar(64) DEFAULT NULL,
  `userface` varchar(255) DEFAULT NULL,
  `status` tinyint(1)  unsigned NOT NULL DEFAULT '1' COMMENT '状态:1-正常;2-删除',
  `create_time` datetime DEFAULT current_timestamp,
  PRIMARY KEY (`id`),
  key `nickname`(`nickname`) using btree,
  key `mobile`(`mobile`) using btree,
  key `status`(`status`) using btree
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'linghu', '令狐葱', '202cb962ac59075b964b07152d234b70', '11111111111','1', 'linghu@qq.com', 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1514093920326&di=44a6fa6b597d86f475c2b15fa93008dd&imgtype=0&src=http%3A%2F%2Fwww.qqzhi.com%2Fuploadpic%2F2015-01-12%2F023019564.jpg', '1','2017-12-08 09:30:22');
INSERT INTO `user` VALUES ('2', 'sang', '江南一点雨', '202cb962ac59075b964b07152d234b70','11111111111', '1', 'sang123@qq.com', 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1514093920321&di=913e88c23f382933ef430024afd9128a&imgtype=0&src=http%3A%2F%2Fp.3761.com%2Fpic%2F9771429316733.jpg', '1','2017-12-21 13:30:29');
INSERT INTO `user` VALUES ('3', 'qiaofeng', '乔峰', '202cb962ac59075b964b07152d234b70', '11111111111','1', 'qiaofeng@qq.com', 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1514093920321&di=913e88c23f382933ef430024afd9128a&imgtype=0&src=http%3A%2F%2Fp.3761.com%2Fpic%2F9771429316733.jpg', '1','2017-12-24 06:30:46');
INSERT INTO `user` VALUES ('4', 'duanzhengchun', '段正淳', '202cb962ac59075b964b07152d234b70','11111111111', '0', 'duanzhengchun@qq.com', 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1514093920321&di=913e88c23f382933ef430024afd9128a&imgtype=0&src=http%3A%2F%2Fp.3761.com%2Fpic%2F9771429316733.jpg', '1','2017-12-24 06:30:46');
INSERT INTO `user` VALUES ('5', 'chenjialuo', '陈家洛', '202cb962ac59075b964b07152d234b70', '11111111111','0', 'chenjialuo@qq.com', 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1514093920321&di=913e88c23f382933ef430024afd9128a&imgtype=0&src=http%3A%2F%2Fp.3761.com%2Fpic%2F9771429316733.jpg', '1','2017-12-24 06:30:46');
INSERT INTO `user` VALUES ('6', 'yuanchengzhi', '袁承志', '202cb962ac59075b964b07152d234b70', '11111111111','1', 'yuanchengzhi@qq.com', 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1514093920321&di=913e88c23f382933ef430024afd9128a&imgtype=0&src=http%3A%2F%2Fp.3761.com%2Fpic%2F9771429316733.jpg', '1','2017-12-24 06:30:46');
INSERT INTO `user` VALUES ('7', 'chuliuxiang', '楚留香', '202cb962ac59075b964b07152d234b70','11111111111', '1', 'chuliuxiang@qq.com', 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1514093920321&di=913e88c23f382933ef430024afd9128a&imgtype=0&src=http%3A%2F%2Fp.3761.com%2Fpic%2F9771429316733.jpg','1', '2017-12-24 06:30:46');
INSERT INTO `user` VALUES ('8', 'baizhantang', '白展堂', '202cb962ac59075b964b07152d234b70', '11111111111','0', 'baizhantang@qq.com', 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1514093920321&di=913e88c23f382933ef430024afd9128a&imgtype=0&src=http%3A%2F%2Fp.3761.com%2Fpic%2F9771429316733.jpg', '1','2017-12-24 06:30:46');
INSERT INTO `user` VALUES ('9', 'renwoxing', '任我行','202cb962ac59075b964b07152d234b70', '11111111111','1', 'renwoxing@qq.com', 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1514093920321&di=913e88c23f382933ef430024afd9128a&imgtype=0&src=http%3A%2F%2Fp.3761.com%2Fpic%2F9771429316733.jpg', '1','2017-12-24 06:30:46');
INSERT INTO `user` VALUES ('10', 'zuolengchan', '左冷禅','202cb962ac59075b964b07152d234b70', '11111111111','1', 'zuolengchan@qq.com', 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1514093920321&di=913e88c23f382933ef430024afd9128a&imgtype=0&src=http%3A%2F%2Fp.3761.com%2Fpic%2F9771429316733.jpg', '1','2017-12-24 06:30:46');
INSERT INTO `user` VALUES ('11', 'fengqingyang', '风清扬','202cb962ac59075b964b07152d234b70', '11111111111','1', 'fengqingyang@qq.com', 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1514093920321&di=913e88c23f382933ef430024afd9128a&imgtype=0&src=http%3A%2F%2Fp.3761.com%2Fpic%2F9771429316733.jpg','1', '2017-12-24 06:30:46');

SET FOREIGN_KEY_CHECKS=1;
