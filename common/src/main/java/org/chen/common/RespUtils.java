package org.chen.common;

import org.chen.respmsg.RespEnum;
import org.chen.vo.RespBean;

public class RespUtils {
    public static RespBean<String> result(RespEnum respEnum){
        return new RespBean<>("",respEnum);
    }

    public static <T> RespBean<T> result(T data, RespEnum respEnum){
        return new RespBean<>(data,respEnum);
    }

    public static RespBean<String> fail(String message){
        return new RespBean<String>(500,"",message);
    }
}
