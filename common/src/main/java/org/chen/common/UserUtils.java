package org.chen.common;

import org.chen.model.User;
import org.springframework.security.core.context.SecurityContextHolder;

public class UserUtils {
    public static Integer getCurrentUserId(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getId();
    }
}
