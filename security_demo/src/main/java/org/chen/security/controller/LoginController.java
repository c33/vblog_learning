package org.chen.security.controller;

import org.chen.common.RespUtils;
import org.chen.respmsg.RespEnum;
import org.chen.vo.RespBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
    @RequestMapping("/login_page")
    public RespBean<String> loginPage(){
        return RespUtils.result(RespEnum.NOT_LOGIN);
    }

    @GetMapping("/hello")
    public RespBean<String> hello(){
        return RespUtils.result("hello welcome!" ,RespEnum.LOGIN_SUCCESS);
    }
}
