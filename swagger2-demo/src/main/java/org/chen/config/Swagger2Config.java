package org.chen.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.function.Predicate;

import static springfox.documentation.builders.PathSelectors.regex;

//@Configuration
//@EnableSwagger2
//@EnableOpenApi
public class Swagger2Config {
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.chen.controller"))
                .paths(helloPaths())
                .build();
    }
    private Predicate<String> helloPaths() {
        return regex(".*/hello.*");
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("vblog")
                .description("vblogapi文档")
                .termsOfServiceUrl("/")
                .version("1.0")
                .build();
    }
}
