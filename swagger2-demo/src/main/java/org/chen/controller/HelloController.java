package org.chen.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//@Api("用户管理接口")
@RestController
public class HelloController {

//    @ApiOperation(value = "获取除了当前用户的所有用户",notes = "获取除了当前用户的所有用户",produces = "form-data")
//    @ApiImplicitParam(name="keyword",value = "用户名称",paramType = "query",required = false,dataType = "String")
    @GetMapping("/admin/hello")
    public String adminHello(String keyword){
        return"hello welcome adminHello! "+keyword;
    }
}
