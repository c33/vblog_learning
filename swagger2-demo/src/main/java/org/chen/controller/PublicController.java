package org.chen.controller;


import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "测试接口")
@RestController
public class PublicController {

    @Parameter(description = "关键字",name = "keyword",required = false,in = ParameterIn.PATH)
    @GetMapping("/public/hello")
    public String hello(String keyword){
        return"hello welcome! "+keyword;
    }
}
