package org.chen.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
//@EnableSwagger2
@EnableOpenApi
public class Swagger2Config{
    @Bean
    public Docket createApi() {
        return new Docket(DocumentationType.SWAGGER_2)
//                .groupName("admin")// 定义组
                .select() // 选择那些路径和api会生成document
                .apis(RequestHandlerSelectors.basePackage("org.chen.web.controller")) // 拦截的包路径
//                .paths(regex("/admin/.*"))// 拦截的接口路径
                .paths(PathSelectors.any())//可以根据url路径设置哪些请求加入文档，忽略哪些请求
                .build() // 创建
                .apiInfo(apiInfo()); // 配置说明
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()//
                .title("vblog")// 设置文档的标题
                .description("vblog仿江南一点雨V部落")// 设置文档的描述
                .termsOfServiceUrl("http://locahost:8080")//设置文档的License信息
                .contact(new Contact("chenss", "http://localhost", "xx@qq.com"))// 联系
                .version("1.0")// 设置文档的版本信息
                .build();
    }

}
