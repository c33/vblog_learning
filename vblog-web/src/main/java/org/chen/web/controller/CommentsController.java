package org.chen.web.controller;

import org.chen.common.RespUtils;
import org.chen.model.Comments;
import org.chen.respmsg.RespEnum;
import org.chen.service.CommentsService;
import org.chen.vo.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/comments")
public class CommentsController {

    @Autowired
    CommentsService service;

    @GetMapping("/")
    public RespBean<List<Comments>> getAllComments(Integer aid){
        List<Comments> list = service.getArticleComments(aid);
        return RespUtils.result(list, RespEnum.SUCCESS);
    }

    @PostMapping("/")
    public RespBean<String> addComments(String content,Integer aid,Integer uid,Integer parentId){
        if (service.addComments(content,aid,uid,parentId)==1){
            return RespUtils.result(RespEnum.INSERT_SUCCESS);
        }
        return RespUtils.result(RespEnum.INSERT_FAIL);
    }
}
