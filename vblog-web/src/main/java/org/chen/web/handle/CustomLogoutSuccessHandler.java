package org.chen.web.handle;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.chen.common.RespUtils;
import org.chen.respmsg.RespEnum;
import org.chen.vo.RespBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setContentType("application/json;charset=UTF-8");
        RespBean<String> result = RespUtils.result( RespEnum.LOGOUT_SUCCESS);
        PrintWriter writer = response.getWriter();
        String resp = new ObjectMapper().writeValueAsString(result);
        writer.write(resp);
        writer.flush();
        writer.close();
    }
}
