package org.chen.web.handle;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

@Component
public class CustomPasswordEncoder implements PasswordEncoder {
    //加密
    @Override
    public String encode(CharSequence rawPassword) {
        //进行MD5加密，DigestUtils是spring提供的工具类
        return DigestUtils.md5DigestAsHex(rawPassword.toString().getBytes());
    }

    /**
     * 数据进行匹配对比
     * @param rawPassword 明文
     * @param encodedPassword 密文
     * @return
     */
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return encodedPassword.equals(DigestUtils.md5DigestAsHex(rawPassword.toString().getBytes()));
    }
}
