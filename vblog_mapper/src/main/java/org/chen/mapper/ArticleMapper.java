package org.chen.mapper;

import org.apache.ibatis.annotations.Param;
import org.chen.model.Article;

import java.util.List;
import java.util.Map;

public interface ArticleMapper {
//    List<Article> getAllArticle(@Param("status")Integer status);
List<Article> getAllArticle(@Param("article") Map<String,Object> map);
    Long getAllArticleCount(@Param("article") Map<String,Object> map);
//    List<Article> getUserArticles(@Param("uid")Integer uid,@Param("status")Integer status);
List<Article> getUserArticles(@Param("article") Map<String,Object> map);
Long getUserArticleCount(@Param("article") Map<String,Object> map);

    Article getArticleById(@Param("id") Integer id);

    Long getArticleCountByCategory(@Param("uid")Integer uid,@Param("cid")Integer cid);

    int addArticle(Article article);
    int updateArticle(Article article);

    int updateArticleStatusById(@Param("aid")Integer aid,@Param("status")Integer status);
    int updateArticleStatusByIds(@Param("aids")String[] aids,@Param("status")Integer status);

    int delArticleByIds(@Param("ids")String[] aids);
}
