package org.chen.mapper;

import org.apache.ibatis.annotations.Param;
import org.chen.model.Article;

import java.util.List;

public interface ArticleTagsMapper {
    int addArticleTags(@Param("aid")Integer aid,@Param("tids")Integer[] tids);
    int delArticleTags(@Param("aid")Integer aid);
}
