package org.chen.mapper;

import org.apache.ibatis.annotations.Param;
import org.chen.model.Category;

import java.util.List;

public interface CategoryMapper {
    List<Category> getAllCategories();

    int addCategory(@Param("name")String name);

    int updateCategoryById(Category category);

    int delCategoryById(@Param("id")Integer id);

    int delCategoryByIds(@Param("ids")Integer[] ids);
}
