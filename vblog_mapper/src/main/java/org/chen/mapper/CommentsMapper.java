package org.chen.mapper;

import org.apache.ibatis.annotations.Param;
import org.chen.model.Comments;

import java.util.List;

public interface CommentsMapper {
    List<Comments> getArticleComments(@Param("aid")Integer aid);

    int addComments(Comments comments);

}
