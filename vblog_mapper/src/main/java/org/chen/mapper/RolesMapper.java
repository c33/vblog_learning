package org.chen.mapper;

import org.apache.ibatis.annotations.Param;
import org.chen.model.Roles;

import java.util.List;

public interface RolesMapper {

    List<Roles> getRolesByUid(@Param("uid")Integer uid);

    List<Roles> getAllRoles();
}
