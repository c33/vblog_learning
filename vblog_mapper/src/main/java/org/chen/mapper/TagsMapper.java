package org.chen.mapper;

import org.apache.ibatis.annotations.Param;
import org.chen.model.Tags;

import java.util.List;

public interface TagsMapper {
    List<Tags> getTagsByUid(@Param("uid")Integer uid);

    List<Tags> getArticleTagsByAid(@Param("aid")Integer aid);

    Long getArticleCountByTag(@Param("tid")Integer tid);

    int addTag(Tags tags);

    int delTag(@Param("ids")String[] ids);

}
