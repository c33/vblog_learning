package org.chen.mapper;

import org.apache.ibatis.annotations.Param;
import org.chen.model.User;

import java.util.List;

public interface UserMapper {
    User loadUserByUsername(@Param("username") String username);
    //获取除了自己以外的用户
    List<User> getAllUsers(@Param("uid")Integer currentUserId,@Param("name") String name);

    User getUserById(@Param("id")Integer id);

    //启用或禁用
    int enableUserById(@Param("id")Integer id,@Param("enabled") Boolean enabled);

    int delUserById(@Param("id")Integer id);
}
