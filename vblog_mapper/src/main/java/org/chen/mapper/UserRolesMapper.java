package org.chen.mapper;

import org.apache.ibatis.annotations.Param;

public interface UserRolesMapper {

    int addUserRolesByUid(@Param("rids")Integer[] rids, @Param("uid")Integer uid);

    int delUserRolesByUid(@Param("uid")Integer uid);
}
