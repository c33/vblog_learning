package org.chen.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class Category {
    private Integer id;
    private String name;

    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "Asia/shanghai")
    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
