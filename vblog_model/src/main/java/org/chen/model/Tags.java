package org.chen.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Tags {
    private Integer id;
    @JsonIgnore
    private Integer uid;
    private String tagName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
