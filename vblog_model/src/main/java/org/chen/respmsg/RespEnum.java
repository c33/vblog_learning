package org.chen.respmsg;

public enum RespEnum {
    SUCCESS(0,"SUCCESS"),
    FAIL(0,"FAIL"),
    LOGIN_SUCCESS(0,"登录成功"),
    LOGIN_FAIL(500,"登录失败"),
    NOT_LOGIN(2,"尚未登录，请登录"),
    LOGOUT_SUCCESS(0,"注销成功"),
    INSERT_SUCCESS(0,"添加成功"),
    INSERT_FAIL(0,"添加失败"),
    UPDATE_SUCCESS(0,"更新成功"),
    UPDATE_FAIL(0,"更新失败"),
    DELETE_SUCCESS(0,"删除成功"),
    DELETE_FAIL(0,"删除失败"),
    UPLOAD_SUCCESS(0,"上传成功"),
    UPLOAD_FAIL(500,"上传失败"),
    ACCESS_DENIED(500,"没有权限");


    private final Integer code;
    private final String message;

    RespEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
