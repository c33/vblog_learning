package org.chen.vo;

import org.chen.respmsg.RespEnum;

public class RespBean<T> {
    private Integer code;
    private T data;
    private String message;

    public RespBean() {
    }

    public RespBean(T date, RespEnum respEnum) {
        this.code = respEnum.getCode();
        this.data = date;
        this.message = respEnum.getMessage();
    }

    public RespBean(Integer code, T date, String message) {
        this.code = code;
        this.data = date;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public RespBean setCode(Integer code) {
        this.code = code;
        return this;
    }

    public T getData() {
        return data;
    }

    public RespBean setData(T data) {
        this.data = data;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public RespBean setMessage(String message) {
        this.message = message;
        return this;
    }
}
