package org.chen.vo;


import java.util.List;

public class RespPageBean{
    private List<?> data;
    private Long count;
    private Long totalPage;
    private Integer page;
    private Integer pageSize;

    public List<?> getData() {
        return data;
    }

    public static RespPageBean build(){
        return new RespPageBean();
    }

    public RespPageBean setData(List<?> data) {
        this.data = data;
        return this;
    }

    public Long getCount() {
        return count;
    }

    public RespPageBean setCount(Long count) {
        this.count = count;
        return this;
    }

    public Long getTotalPage() {
        double total =this.count/(double)this.pageSize;
        totalPage= ((long) Math.ceil(total));
        return totalPage;
    }


    public Integer getPage() {
        return page;
    }

    public RespPageBean setPage(Integer page) {
        this.page = page;
        return this;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
