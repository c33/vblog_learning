package org.chen.vo;

public class TagsInfo {
    private Integer id;
    private String tagName;
    private Long articleCount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Long getArticleCount() {
        if (articleCount==null){
            articleCount=0L;
        }
        return articleCount;
    }

    public void setArticleCount(Long articleCount) {
        this.articleCount = articleCount;
    }
}
