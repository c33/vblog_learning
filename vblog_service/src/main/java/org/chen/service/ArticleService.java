package org.chen.service;

import org.chen.common.HttpStripUtils;
import org.chen.common.UserUtils;
import org.chen.mapper.ArticleMapper;
import org.chen.mapper.ArticleTagsMapper;
import org.chen.model.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ArticleService {
    @Autowired
    ArticleMapper mapper;

    @Autowired
    ArticleTagsMapper tagsMapper;

    public List<Article> getAllArticle(String keywords, Integer page, Integer count) {
        Map<String, Object> map = new HashMap<>();
        map.put("keywords", keywords);
        if (page == null || page == 0) {
            page = 1;
        }
        Integer pageIndex = count * (page - 1);
        map.put("pageIndex", pageIndex);
        map.put("pageSize", count);
        return mapper.getAllArticle(map);
    }

    public Long getAllArticleCount(String keywords) {
        Map<String, Object> map = new HashMap<>();
        map.put("keywords", keywords);
        return mapper.getUserArticleCount(map);
    }

    public List<Article> getUserArticles(String keywords, Integer status, Integer page, Integer count) {
        Map<String, Object> map = new HashMap<>();
        map.put("keywords", keywords);
        map.put("status", status);
        map.put("uid", UserUtils.getCurrentUserId());
        if (page == null || page == 0) {
            page = 1;
        }
        Integer pageIndex = count * (page - 1);
        map.put("pageIndex", pageIndex);
        map.put("pageSize", count);
        return mapper.getUserArticles(map);
    }

    public Long getUserArticleCount(String keywords, Integer status) {
        Map<String, Object> map = new HashMap<>();
        map.put("keywords", keywords);
        map.put("status", status);
        map.put("uid", UserUtils.getCurrentUserId());
        return mapper.getUserArticleCount(map);
    }

    public Article getArticleById(Integer id) {
        return mapper.getArticleById(id);
    }

    @Transactional
    public int addArticle(String title, String mdContent, String htmlContent, Integer cid, Integer status, Integer[] tid) {

        Article article = new Article();
        article.setTitle(title);
        article.setMdContent(mdContent);
        article.setHtmlContent(htmlContent);
        String html = HttpStripUtils.stripHtml(article.getHtmlContent());
        article.setSummary(html.length() > 50 ? html.substring(0, 50) : html);
        article.setUid(UserUtils.getCurrentUserId());
        article.setCid(cid);
        article.setStatus(status);
        int result = mapper.addArticle(article);
        if (tid != null || tid.length != 0) {
            tagsMapper.addArticleTags(article.getId(), tid);
        }

        return result;
    }

    public int updateArticleStatusById(Integer aid, Integer status) {
        return mapper.updateArticleStatusById(aid, status);
    }

    public boolean updateArticleStatusByIds(String aid, Integer status) {
        String[] aids=aid.split(",");
        int result = mapper.updateArticleStatusByIds(aids, status);
        return result==aids.length;
    }

    @Transactional
    public int updateArticle(Integer id, String title, String mdContent, String htmlContent, Integer cid, Integer status, Integer[] tid) {
        Article article = new Article();
        article.setId(id);
        article.setTitle(title);
        article.setMdContent(mdContent);
        article.setHtmlContent(htmlContent);
        String html = HttpStripUtils.stripHtml(article.getHtmlContent());
        article.setSummary(html.length() > 50 ? html.substring(0, 50) : html);
        article.setUid(UserUtils.getCurrentUserId());
        article.setCid(cid);
        article.setStatus(status);
        if (tid != null || tid.length != 0) {
            tagsMapper.delArticleTags(article.getId());
            tagsMapper.addArticleTags(article.getId(), tid);
        }
        int result = mapper.updateArticle(article);
        return result;
    }

    public Long getArticleCountByCategory(Integer cid) {
        return mapper.getArticleCountByCategory(UserUtils.getCurrentUserId(), cid);
    }

    public boolean delArticleByIds(String ids){
        String[] aids=ids.split(",");
        int result = mapper.delArticleByIds(aids);
        return result==aids.length;
    }
}
