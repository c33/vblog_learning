package org.chen.service;

import org.chen.mapper.CategoryMapper;
import org.chen.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {
    @Autowired
    CategoryMapper mapper;

    public List<Category> getAllCategories(){
        return mapper.getAllCategories();
    }

    public int addCategory(String name){
        return mapper.addCategory(name);
    }

    public int updateCategoryById(Integer id,String name){
        Category category=new Category();
        category.setId(id);
        category.setName(name);
        return mapper.updateCategoryById(category);
    }
    public int delCategoryById(Integer id){
        return mapper.delCategoryById(id);
    }

    public int delCategoryByIds(Integer[] ids){
        return mapper.delCategoryByIds(ids);
    }
}
