package org.chen.service;

import org.chen.mapper.CommentsMapper;
import org.chen.model.Comments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentsService {

    @Autowired
    CommentsMapper mapper;

   public List<Comments> getArticleComments(Integer aid) {
    return mapper.getArticleComments(aid);
   }

   public int addComments(String content,Integer aid,Integer uid,Integer parentId) {
        Comments comments = new Comments();
        comments.setAid(aid);
        comments.setContent(content);
        comments.setUid(uid);
        if (parentId==null){
            comments.setParentId(0);
        }else {
            comments.setParentId(parentId);
        }
        return mapper.addComments(comments);
    }

}
