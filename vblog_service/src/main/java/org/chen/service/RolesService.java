package org.chen.service;

import org.chen.mapper.RolesMapper;
import org.chen.model.Roles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RolesService {

    @Autowired
    RolesMapper mapper;

    public List<Roles> getRolesByUid(Integer uid){
        return mapper.getRolesByUid(uid);
    }

    public List<Roles> getAllRoles(){
        return mapper.getAllRoles();
    }
}
