package org.chen.service;

import org.chen.common.UserUtils;
import org.chen.mapper.TagsMapper;
import org.chen.model.Tags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagsService {
    @Autowired
    TagsMapper mapper;

    public List<Tags> getTagsByUid() {
        return mapper.getTagsByUid(UserUtils.getCurrentUserId());

    }

    public List<Tags> getArticleTagsByAid(Integer aid) {
        return mapper.getArticleTagsByAid(aid);

    }

    public int addTag( String tagName) {
        Tags tags = new Tags();
        tags.setUid(UserUtils.getCurrentUserId());
        tags.setTagName(tagName);
        return mapper.addTag(tags);
    }

    public boolean delTag(String ids) {
        String[] idList=ids.split(",");
        return mapper.delTag(idList)==idList.length;
    }

    public Long getArticleCountByTag(Integer tid) {
        return mapper.getArticleCountByTag(tid);
    }
}
